import bcrypt from 'bcryptjs';
import User from '../models/user';

class AuthController {
    constructor() {
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
    }

    //getLoginForm to get the login form for the user
    async getLoginForm(req, res) {
        return res.render('pages/login');
    }

    //login() - to authenticate the user
    async login(req, res) {
        let {username, password} = req.body;
        const user = await User.findOne({username}).exec();

        if (user && bcrypt.compareSync(password, user.password)) {
            return res.redirect('/order/all');
        }

        return res.render('pages/login', {
            errorMessage: "Wrong username or password"
        });
    }

    // logout() - to remove the user from access
    async logout(req, res) {
        return res.redirect('/order');
    }
}

export default AuthController

