import Order from '../models/order';

const shippingPrices = {
    d1: 40.,
    d2: 30.,
    d3: 20.,
    d4: 10.
}

class OrderController {
    getOrderForm(req, res) {
        return res.render('index');
    }

    async postOrder(req, res) {
        try {
            req.body.shippingCharges = req.body.delivery && req.body.delivery in shippingPrices
                ? shippingPrices[req.body.delivery]
                : 0;
            delete req.body.delivery;

            // ORDER.CREATE
            const order = await Order.create(req.body);

            return res.render('pages/receipt', {order});
        } catch (e) {
            return res.render('index', {errors: e.errors});
        }
    }

    async getAllOrders(req, res) {
        return res.render('pages/orders', {orders: await Order.find().lean()});
    }
}

export default OrderController