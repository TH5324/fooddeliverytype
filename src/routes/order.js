import express from 'express';

import OrderController from "../controllers/order";

const ORDER_PREFIX = '/order';
const router = express.Router();
const orderController = new OrderController();

router
    .get('/', orderController.getOrderForm)
    .get('/all', orderController.getAllOrders)
    .post('/', orderController.postOrder);

export default router
export {ORDER_PREFIX}