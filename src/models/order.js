import mongoose from 'mongoose';

const productPrices = {
    product1: 8.99,
    product2: 22.99,
    product3: 19.50,
    product4: 15.
}

const provinceTaxes = {
    "AB": .05,
    "BC": .12,
    "MB": .12,
    "NB": .15,
    "NL": .15,
    "NS": .15,
    "NT": .05,
    "NU": .05,
    "ON": .13,
    "PE": .15,
    "SK": .11,
    "QC": .14975,
    "YT": .05
}

const orderSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'Name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
        match: [/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/, 'Email must be in format test@test.com']
    },
    phone: {
        type: String,
        required: [true, 'Phone Number is required'],
        match: [/^([0-9]){3}-([0-9]){3}-([0-9]){4}$/, 'Phone must be in format xxx-xxx-xxxx']
    },
    address: {
        type: String,
        required: [true, 'Address is required']
    },
    city: {
        type: String,
        required: [true, 'City is required']
    },
    postal: {
        type: String,
        required: [true, 'Postal Code is required'],
        match: [/^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$/i, 'Postal code must be in format X9X X9X']
    },
    province: {
        type: String,
        required: [true, 'Province is required']
    },

    product1: Number,
    product2: Number,
    product3: Number,
    product4: Number,

    product1cost: Number,
    product2cost: Number,
    product3cost: Number,
    product4cost: Number,

    shippingCharges: Number,
    subTotal: Number,
    taxes: Number,
    total: Number
});

orderSchema.pre('save', function (next) {
    const order = this;

    order.product1cost = order.product1 ? order.product1 * productPrices.product1 : 0;
    order.product2cost = order.product2 ? order.product2 * productPrices.product2 : 0;
    order.product3cost = order.product3 ? order.product3 * productPrices.product3 : 0;
    order.product4cost = order.product4 ? order.product4 * productPrices.product4 : 0;

    if (order.product1cost + order.product2cost + order.product3cost + order.product4cost < 10) {
        const error = new mongoose.Error.ValidationError(this);
        error.errors.common = new mongoose.Error.ValidatorError({message: 'Minimum purchase should be of $10'});
        return next(error);
    }

    order.subTotal = order.product1cost
        + order.product2cost
        + order.product3cost
        + order.product4cost
        + order.shippingCharges;

    order.taxes = order.subTotal * provinceTaxes[order.province];
    order.total = order.subTotal + order.taxes;

    next();
});

const Order = mongoose.model('Order', orderSchema, 'orders');

export default Order;