import User from './user';

export default async function createInitData() {
    if ((await User.find()).length === 0) {
        await User.create({ // user login details
            username: 'admin',
            email: 'admin@test.ca',
            password: 'admin'
        });
    }

}