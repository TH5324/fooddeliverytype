import path from 'path';
import express from 'express';
import mongoose from "mongoose";
import bodyParser from 'body-parser';
import orderRouter, {ORDER_PREFIX} from './routes/order';
import authRouter, {AUTH_PREFIX} from './routes/auth';
import createInitData from "./models/initializeDB";

mongoose.connect('mongodb://localhost:27017/PrimeSpot', {useNewUrlParser: true, useUnifiedTopology: true});

createInitData().then(() => console.log('Database created and initialized'));

let app = express();

app.set('views', path.join(path.resolve(), 'src', 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({extended: false}));

app.use('/static', express.static(path.join(path.resolve(), 'src', 'styles')));

app.use('/images', express.static(path.join(path.resolve(), 'src', 'images')));

app.use(ORDER_PREFIX, orderRouter);
app.use(AUTH_PREFIX, authRouter);

export default app;
